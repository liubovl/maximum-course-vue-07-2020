
// 2.1) вывести n первых чисел Фибоначчи 
function fib(n) {
	let a = 1;
	let b = 1;
	let c;
	let arr = [];
	if (n <= 0) {
		console.log ("Не может быть меньше или равно 0");
	}
	else if (n == 1) {
		arr.push(a);
	}
	else {
		arr.push(a);
		arr.push(b);
		for (i = 3; i <= n ; i++) {
			c = a + b;
			arr.push(c);
			a = b;
			b = c;
		}
	}
	return arr;
};

console.log(fib(10));



// 2.2) написать алгоритм быстрой сортировки (quick sort)

let arr = [1000, 3, 45, 7, 1, 90, 6, 8];

const qsort = (arr) => {
  if (arr.length < 2){
    return arr;
  }
  else {
    let pivot = arr[Math.floor(Math.random() * arr.length)];
    let less = arr.filter((value) => {
      return value < pivot;
    });
    let greater = arr.filter((value) => {
      return value > pivot;
    });
    return [].concat(qsort(less), pivot, qsort(greater));
  }
};

console.log(qsort(arr));